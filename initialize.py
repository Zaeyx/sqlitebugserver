#!/usr/bin/env python
import random
import hashlib
import os
import sys
import subprocess

mynum = ''

if ".db" in subprocess.check_output("ls -alh | grep .db", shell=True):
	print "It appears the db is already initialized."
	print "Quitting.."
	exit(-1)

for i in range(100):
	mynum = str(random.randint(1,65535)) + mynum


value = hashlib.md5(mynum).hexdigest()

dbname = "."+value+".db"

webroot = "/var/www/"
old_webroot = webroot

rundir = os.path.dirname(os.path.realpath(__file__))

COM1 = """
sqlite3 """+dbname+""" << XSS
create table requests (id text, type text, ip_address text, user_agent text, time INT(11));

XSS
"""

COM2 = """
cat index.php | sed "s/\$dbname =.*/\$dbname = '""" + dbname +  """';/g" > """+value

COM3 = """
mv """+value+""" index.php"""

while True:
	webroot = raw_input("Webroot is? [Default: %s]>> " % (webroot))

	if len(webroot) < 1:
		webroot = old_webroot

	choice = raw_input("Webroot=%s, correct? [y/N]>>" % (webroot))
	if choice.lower() == "y":
		break

if webroot[len(webroot)-1] != "/":
	webroot += "/sqlitebugserver"
else:
	webroot += "sqlitebugserver"

COM4 = """
ln -s """ + rundir + """ """ + webroot + """ && chown www-data:www-data -R """ + rundir + """
"""


print COM1, "\n\n", COM2, "\n\n", COM3, "\n\n", COM4, "\n\n", dbname

os.system(COM1)
os.system(COM2)
os.system(COM3)
os.system(COM4)
