#!/usr/bin/env python

import os
import subprocess

dir_path = os.path.dirname(os.path.realpath(__file__))

if not dir_path[len(dir_path)-1] == "/":
	dir_path += "/"

subprocess.check_output("find %s -name '.*.db' -exec rm {} +" % (dir_path),shell=True)
