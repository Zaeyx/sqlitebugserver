#!/usr/bin/env python

import os
import subprocess

dir_path = os.path.dirname(os.path.realpath(__file__))

subprocess.call("find %s -name '.*.db' -exec sqlite3 {} \;" % (dir_path), shell=True)
