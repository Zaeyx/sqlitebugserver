SqliteBugServer consists of a php page that takes requests in a specific format, logs details about the requests to a database, and returns the type of media file requested.

SqliteBugServer uses a Sqlite database.  Run ./initialize.py to create it.

The structure is as follows:
CREATE TABLE requests (id TEXT, type TEXT, ip_address TEXT, user_agent TEXT, time INTEGER);


